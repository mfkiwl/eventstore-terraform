#!/usr/bin/env bash

set -euxo pipefail

sudo apt-get update
sudo DEBIAN_FRONTEND=noninteractive apt-get dist-upgrade -y
sudo snap refresh

# Disable unattended upgrades
sudo sed -i 's/^APT::Periodic::Unattended-Upgrade "1";/APT::Periodic::Unattended-Upgrade "0";/g' /etc/apt/apt.conf.d/20auto-upgrades

# TODO: Sometimes, for some reason zip won't install without another update.
# The problem is intermittent, so it's not even certain that a second update
# fixes it.
sudo apt-get update
sudo apt-get install -y zip

sudo apt-get install -y libwww-perl libdatetime-perl
sudo apt-get install -y python3
sudo apt-get install -y python3-pip
sudo apt-get install -y jq
sudo apt-get install -y awscli
sudo apt-get install -y zfsutils-linux
sudo apt-get install -y iperf3

sudo -H pip3 install aws-ec2-assign-elastic-ip==0.10.2
sudo -H pip3 install credstash==1.17.1

# cloudwatch monitoring scripts
curl -so /tmp/CloudWatchMonitoringScripts-1.2.1.zip http://aws-cloudwatch.s3.amazonaws.com/downloads/CloudWatchMonitoringScripts-1.2.1.zip
sudo unzip -o -d /opt /tmp/CloudWatchMonitoringScripts-1.2.1.zip
echo '*/5 * * * * root /opt/aws-scripts-mon/mon-put-instance-data.pl --mem-util --mem-used --mem-avail --disk-space-util --disk-path=/ --from-cron' | sudo tee /etc/cron.d/cloudwatch
echo '*/5 * * * * root /opt/aws-scripts-mon/mon-put-instance-data.pl --disk-space-util --disk-path=/var/lib/eventstore --from-cron --auto-scaling' | sudo tee -a /etc/cron.d/cloudwatch


# cloudwatch log exporter
curl -so /tmp/amazon-cloudwatch-agent.deb https://s3.amazonaws.com/amazoncloudwatch-agent/ubuntu/amd64/latest/amazon-cloudwatch-agent.deb 
sudo dpkg -i /tmp/amazon-cloudwatch-agent.deb
sudo /opt/aws/amazon-cloudwatch-agent/bin/amazon-cloudwatch-agent-ctl -a fetch-config -m ec2 -s -c file:/tmp/files/cloudwatch-log-config.json

# SSM agent
sudo systemctl enable snap.amazon-ssm-agent.amazon-ssm-agent.service
sudo systemctl start snap.amazon-ssm-agent.amazon-ssm-agent.service

curl -s https://packagecloud.io/install/repositories/EventStore/EventStore-OSS/script.deb.sh | sudo bash

sudo tee -a /etc/security/limits.conf > /dev/null << EOF
*               soft    nofile          100000
*               hard    nofile          100000
root            soft    nofile          100000
root            hard    nofile          100000
EOF

sudo apt-get install -y eventstore-oss=4.1.1-hotfix1-1
sudo apt-get install -y mono-devel

