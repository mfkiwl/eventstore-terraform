#!/bin/bash
set -e
CLUSTER_SIZE=3
REGION=ap-southeast-2
script="$(basename "${BASH_SOURCE[0]}")"

function printUsage {
    cat << EOF
Usage: $script OPTIONS
where OPTIONS are
   -p, --prefix
        credstash key prefix, default "dev-eventstore"
   -u, --url
        cluster url, default "https://dev-eventstore.geodesy.ga.gov.au"
EOF
}

prefix="dev-eventstore"
url="https://dev-eventstore.geodesy.ga.gov.au"
while [[ $# -gt 0 ]]; do
    case $1 in
        -p|--prefix )
            prefix="$2"
            shift 2
            ;;
        -u|--url )
            url="$2"
            shift 2
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
        ;;
    esac
done

echo "Checking if cluster is up"
servers="0"
masters="0"
until [ "$servers" -ge "$CLUSTER_SIZE" ] && [ "$masters" == "1" ]; do
    servers=$(curl --silent --connect-timeout 1 $url/gossip | jq '[.members[] | select(.isAlive) ] | length ')
    masters=$(curl --silent --connect-timeout 1 $url/gossip | jq '[.members[] | select(.isAlive) | select(.state == "Master") ] | length')
    echo "Waiting for cluster: $servers servers up, $masters master"
    sleep 5
done

echo "Cluster is up."

# Get the password, if it doesn't exist use default
oldPassword=$(credstash -r $REGION get "$prefix"AdminPassword)
rc=$?; 
if [[ $rc != 0 ]]; then 
    oldPassword="changeit"
fi

echo "Checking if cluster is writable"
# Wait until we can write
writable=false
until [ $writable == true ]; do
    # Try a write, output the HTTP Response Code, fail >400
    http_code=$(curl -f -s -o /dev/null -w "%{http_code}" \
        -d "{}" "$url/streams/test" \
        -u "admin:$oldPassword" \
        -H "Content-Type:application/json" \
        -H "ES-EventType: SomeEvent" \
        -H "ES-EventId: B322E299-CB73-4B47-97C5-5054F920746F")
    rc=$?;
    if [[ $rc == 0 ]]; then
        echo "Cluster is writable"
        writable=true
    else
        echo "$http_code Error, cluster is not writable yet"
        if [ $http_code == "401" ]; then
            echo "Credential Error, attempting to get new credentials"
            oldPassword=$(credstash -r $REGION get "$prefix"AdminPassword)
        fi
        sleep 10
    fi
done