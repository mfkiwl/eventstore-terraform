#==============================================================
# main.tf
#==============================================================

#--------------------------------------------------------------
# Global Config
#--------------------------------------------------------------

# Terraform Configuration

provider "archive" {
  version = "1.3.0"
}

provider "aws" {
  region  = var.region
  version = "3.6.0"
}

provider "template" {
  version = "2.1.2"
}

terraform {
  required_providers {
    archive = {
      source = "hashicorp/archive"
    }
    aws = {
      source = "hashicorp/aws"
    }
    template = {
      source = "hashicorp/template"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    # Add encryption to the bucket (not overwritten as part of deploy.sh)
    encrypt = true
  }
}

locals {
  domain_name = var.environment == "prod" ? "eventstore.geodesy.ga.gov.au" : "${var.environment}-eventstore.geodesy.ga.gov.au"
}

# Add a record for all the eips in this cluster
resource "aws_route53_record" "nodes" {
  count                            = var.elastic_ip_count
  zone_id                          = data.aws_route53_zone.selected.zone_id
  name                             = "nodes"
  type                             = "A"
  records                          = [element(aws_eip.eips.*.public_ip, count.index)]
  set_identifier                   = "${var.stack}-${count.index}"
  multivalue_answer_routing_policy = true
  ttl                              = "300"

  depends_on = [aws_eip.eips]
}

# Add a record for the eips in our stack
resource "aws_route53_record" "stack" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = "${var.stack}-nodes"
  type    = "A"
  ttl     = "300"
  records = aws_eip.eips.*.public_ip
}

#--------------------------------------------------------------
# Locals
#--------------------------------------------------------------

# Locals can be used to combine variables and make them easier
# to reference

locals {
  name_tag_prefix = "${var.environment}-${var.stack}-${var.application}"
}

#--------------------------------------------------------------
# Data
#--------------------------------------------------------------

# Data allows us to reference resources out of the scope of this
# Terraform script

data "aws_caller_identity" "current" {
}

data "aws_route53_zone" "selected" {
  name = local.domain_name
}

resource "aws_acm_certificate" "certificate" {
  domain_name = "*.${local.domain_name}"

  subject_alternative_names = [local.domain_name]

  validation_method = "DNS"

  lifecycle {
    create_before_destroy = true
  }

  tags = {
    Name = local.name_tag_prefix
  }
}

resource "aws_route53_record" "certificate" {
  for_each = {
    for option in aws_acm_certificate.certificate.domain_validation_options : option.domain_name => {
      name   = option.resource_record_name
      record = option.resource_record_value
      type   = option.resource_record_type
    }
  }

  name    = each.value.name
  records = [each.value.record]
  ttl     = 60
  type    = each.value.type
  zone_id = data.aws_route53_zone.selected.zone_id

  allow_overwrite = true
}

resource "aws_acm_certificate_validation" "certificate" {
  certificate_arn = aws_acm_certificate.certificate.arn

  validation_record_fqdns = [for record in aws_route53_record.certificate : record.fqdn]
}

# Credstash Resources

data "aws_kms_alias" "credstash" {
  name = "alias/credstash"
}

data "aws_dynamodb_table" "credstash" {
  name = "credential-store"
}
