#==============================================================
# output.tf
#==============================================================

# Defines what to write to the console after running

output "dns_address" {
  value = "https://${aws_route53_record.elb.fqdn}/web/index.html"
}
