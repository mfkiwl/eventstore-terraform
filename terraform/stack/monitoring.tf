resource "aws_cloudwatch_metric_alarm" "eventstore_diskspace_low" {
  alarm_name          = "${local.name_tag_prefix}-alarm-diskspace-low"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "2"
  metric_name         = "DiskSpaceUtilization"
  namespace           = "System/Linux"
  period              = "300"
  statistic           = "Maximum"
  threshold           = "80"
  alarm_description   = "This metric monitors if eventstore autoscaling group used disk space is greater than 80%"
  alarm_actions = [
    aws_sns_topic.eventstore_notifications.arn,
  ]
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.node.name
    Filesystem           = "rpool/var-lib-eventstore"
    MountPath            = "/var/lib/eventstore"
  }
}

resource "aws_cloudwatch_metric_alarm" "unhealthy_hosts" {
  alarm_name          = "${local.name_tag_prefix}-alarm-unhealthy-hosts"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "UnHealthyHostCount"
  namespace           = "AWS/ELB"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "0"
  alarm_description   = "This metric monitors if the eventstore load balancer has unhealthy hosts"
  alarm_actions = [
    aws_sns_topic.eventstore_notifications.arn,
  ]
  dimensions = {
    LoadBalancerName = aws_elb.elb.name
  }
}

# instance status check = if instance OS is accepting traffic (e.g., misconfiguration, like network interface disabled)
resource "aws_cloudwatch_metric_alarm" "status_check_failed_instance" {
  alarm_name          = "${local.name_tag_prefix}-alarm-status-check-instance"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "StatusCheckFailed_Instance"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "0"
  alarm_description   = "This metric monitors if the eventstore autoscaling group instance status-check fails"
  alarm_actions = [
    aws_sns_topic.eventstore_notifications.arn,
  ]
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.node.name
  }
}

# system status check = if instance is reachable by network (for AWS power, infrastructure issues)
resource "aws_cloudwatch_metric_alarm" "status_check_failed_system" {
  alarm_name          = "${local.name_tag_prefix}-alarm-status-check-system"
  comparison_operator = "GreaterThanThreshold"
  evaluation_periods  = "1"
  metric_name         = "StatusCheckFailed_System"
  namespace           = "AWS/EC2"
  period              = "60"
  statistic           = "Maximum"
  threshold           = "0"
  alarm_description   = "This metric monitors if the eventstore autoscaling group system status-check fails"
  alarm_actions = [
    aws_sns_topic.eventstore_notifications.arn,
  ]
  dimensions = {
    AutoScalingGroupName = aws_autoscaling_group.node.name
  }
}

resource "aws_autoscaling_notification" "notify_autoscale" {
  group_names = [
    aws_autoscaling_group.node.name,
  ]

  notifications = [
    "autoscaling:EC2_INSTANCE_LAUNCH",
    "autoscaling:EC2_INSTANCE_TERMINATE",
    "autoscaling:EC2_INSTANCE_LAUNCH_ERROR",
  ]

  topic_arn = aws_sns_topic.eventstore_notifications.arn
}

resource "aws_sns_topic" "eventstore_notifications" {
  name = "${local.name_tag_prefix}-topic-eventstore-notifications"
}

resource "aws_sns_topic_subscription" "eventstore_notification" {
  topic_arn = aws_sns_topic.eventstore_notifications.arn
  protocol  = "lambda"
  endpoint  = aws_lambda_function.slack_notifications.arn
}

resource "aws_s3_bucket" "lambda_source_bucket" {
  bucket = "${local.name_tag_prefix}-lambda-source"
}

data "archive_file" "eventstore_diskspace_low" {
  type        = "zip"
  source_file = "slack_webhook_lambda.js"
  output_path = "slack_webhook_lambda.zip"
}

resource "aws_s3_bucket_object" "slack_webhook" {
  bucket = aws_s3_bucket.lambda_source_bucket.id
  key    = "${local.name_tag_prefix}-slack_webhook_lambda.zip"
  source = data.archive_file.eventstore_diskspace_low.output_path
  etag   = data.archive_file.eventstore_diskspace_low.output_md5
}

resource "aws_iam_role" "lambda_role" {
  name               = "${local.name_tag_prefix}-lambda-role"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "lambda.amazonaws.com"
      },
      "Effect": "Allow"
    }
  ]
}
EOF

}

resource "aws_iam_role_policy" "cloudwatch_lambda_policy" {
  name = "${local.name_tag_prefix}-cloudwatch"
  role = aws_iam_role.lambda_role.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    }
  ]
}
EOF
}

resource "aws_lambda_function" "slack_notifications" {
  s3_bucket         = aws_s3_bucket_object.slack_webhook.bucket
  s3_key            = aws_s3_bucket_object.slack_webhook.key
  s3_object_version = aws_s3_bucket_object.slack_webhook.version_id
  source_code_hash  = data.archive_file.eventstore_diskspace_low.output_md5
  function_name     = "${local.name_tag_prefix}-sns-topic-lambda"
  role              = aws_iam_role.lambda_role.arn
  handler           = "slack_webhook_lambda.handler"
  runtime           = "nodejs12.x"

  environment {
    variables = {
      slack_webhook_path = var.slack_webhook_path
    }
  }
}

resource "aws_lambda_permission" "eventstore_notifications" {
  statement_id  = "AllowExecutionFromSNS"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.slack_notifications.arn
  principal     = "sns.amazonaws.com"
  source_arn    = aws_sns_topic.eventstore_notifications.arn
}
