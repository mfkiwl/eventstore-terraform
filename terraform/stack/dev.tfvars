# https://bitbucket.org/geoscienceaustralia/eventstore-terraform/addon/pipelines/home#!/results/413
ami = "ami-0222fbd700e2721be" 

key_name = "geodesy-ops-dev-eventstore"

availability_zones = ["ap-southeast-2a", "ap-southeast-2b"]
subnet_cidrs = ["10.1.1.0/24", "10.1.2.0/24"]
elastic_ip_count = 2
