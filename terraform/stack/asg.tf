#==============================================================
# asg.tf
#==============================================================

# Creates The launch configuration, backup bucket and instance
# Profiles. Finally launches them in an ASG

#--------------------------------------------------------------
# Instance Profile
#--------------------------------------------------------------

# The instance profile is a role that can be assigned to nodes
# This manages how our server interacts with AWS services

resource "aws_iam_instance_profile" "node" {
  role = aws_iam_role.node.name
}

resource "aws_iam_role" "node" {
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ec2.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
EOF
}

data "aws_iam_policy" "ssm" {
  arn = "arn:aws:iam::aws:policy/service-role/AmazonEC2RoleforSSM"
}

resource "aws_iam_role_policy_attachment" "ssm" {
  role       = aws_iam_role.node.id
  policy_arn = data.aws_iam_policy.ssm.arn
}

resource "aws_iam_role_policy" "node" {
  role = aws_iam_role.node.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect":"Allow",
            "Action":[
                "ec2:DescribeTags",
                "ec2:DescribeAddresses",
                "ec2:AssociateAddress"
            ],
            "Resource":"*",
            "Condition":{
                "Bool":{
                    "aws:SecureTransport":"true"
                 }
            }
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:PutObject*",
                "s3:GetObject*"
            ],
            "Resource": ["arn:aws:s3:::${var.environment}-eventstore-backups-${data.aws_caller_identity.current.account_id}/*"]
        },
        {
            "Effect": "Allow",
            "Action": [
                "s3:ListObjects",
                "s3:ListBucket"
            ],
            "Resource": ["arn:aws:s3:::${var.environment}-eventstore-backups-${data.aws_caller_identity.current.account_id}"]
        }
    ]
}
EOF
}

resource "aws_iam_role_policy" "credstash" {
  role = aws_iam_role.node.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "kms:GenerateDataKey",
        "kms:Decrypt"
      ],
      "Effect": "Allow",
      "Resource": "arn:aws:kms:${var.region}:${data.aws_caller_identity.current.account_id}:key/${data.aws_kms_alias.credstash.target_key_id}"
    },
    {
      "Action": [
        "dynamodb:PutItem",
        "dynamodb:GetItem",
        "dynamodb:Query",
        "dynamodb:Scan"
      ],
      "Effect": "Allow",
      "Resource": "${data.aws_dynamodb_table.credstash.arn}"
    }
  ]
}
EOF
}

# The policy to allow cloudwatch to send memory metrics
resource "aws_iam_role_policy" "cloudwatch_policy" {
  role = aws_iam_role.node.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "cloudwatch:PutMetricData",
        "cloudwatch:GetMetricStatistics",
        "cloudwatch:ListMetrics",
        "ec2:DescribeTags"
      ],
      "Resource": "*"
    }
  ]
}
EOF
}

# Allow us to export logs to cloudwatch
resource "aws_iam_role_policy" "cloudwatch_logs_policy" {
  role = aws_iam_role.node.id

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "logs:CreateLogGroup",
        "logs:CreateLogStream",
        "logs:PutLogEvents",
        "logs:DescribeLogStreams"
      ],
      "Resource": [
        "arn:aws:logs:*:*:*"
      ]
    }
  ]
}
EOF
}

# Allow us to run ssm commands
resource "aws_iam_role_policy" "ssm_policy" {
  role = aws_iam_role.node.id

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": [
                "ssm:DescribeAssociation",
                "ssm:GetDeployablePatchSnapshotForInstance",
                "ssm:GetDocument",
                "ssm:GetParameters",
                "ssm:ListAssociations",
                "ssm:ListInstanceAssociations",
                "ssm:PutInventory",
                "ssm:UpdateAssociationStatus",
                "ssm:UpdateInstanceAssociationStatus",
                "ssm:UpdateInstanceInformation"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2messages:AcknowledgeMessage",
                "ec2messages:DeleteMessage",
                "ec2messages:FailMessage",
                "ec2messages:GetEndpoint",
                "ec2messages:GetMessages",
                "ec2messages:SendReply"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ec2:DescribeInstanceStatus"
            ],
            "Resource": "*"
        },
        {
            "Effect": "Allow",
            "Action": [
                "ds:CreateComputer",
                "ds:DescribeDirectories"
            ],
            "Resource": "*"
        }
    ]
}
EOF
}

# resource "aws_cloudwatch_log_group" "eventstore" {
#   name              = "${local.name_tag_prefix}-eventstore"
#   retention_in_days = "30"
# }

#--------------------------------------------------------------
# Security Group
#--------------------------------------------------------------

# The security group acts as a simple firewall around our nodes

resource "aws_security_group" "node" {
  vpc_id = aws_vpc.main.id

  ingress {
    description = "SSH From anywhere"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"] # TODO
  }

  ingress {
    description = "External HTTP - HTTP Client Access"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Internal HTTP - Cluster elections and gossip"
    from_port   = 2112
    to_port     = 2112
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "Internal TCP - Cluster replication and forwarding"
    from_port   = 1112
    to_port     = 1112
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "External Secure TCP - TCP Client Access"
    from_port   = 5432
    to_port     = 5432
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    description = "iperf"
    from_port   = 5201
    to_port     = 5201
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.main.cidr_block]
  }

  egress {
    description = "Egress to anywhere"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#--------------------------------------------------------------
# Userdata script
#--------------------------------------------------------------

# Use the configure-eventstore.sh as a template and inject tf
# variables into it

data "template_file" "configure-eventstore-script" {
  template = file("${path.module}/configure-eventstore.sh")

  vars = {
    environment             = var.environment
    eips                    = join(" ", aws_eip.eips.*.public_ip)
    backup_bucket           = "${var.environment}-eventstore-backups-${data.aws_caller_identity.current.account_id}"
    name_tag_prefix         = "${var.environment}-${var.application}"
    region                  = var.region
    cluster_dns             = aws_route53_record.nodes[0].fqdn
    tcp_dns                 = "*.${replace(data.aws_route53_zone.selected.name, "au.", "au")}"
    database_disk_device    = var.database_disk_device
    database_disk_partition = var.database_disk_partition
  }
}

#--------------------------------------------------------------
# Launch Configuration
#--------------------------------------------------------------

# Load all of the configurations above into a re-usable template

resource "aws_launch_configuration" "node" {
  name_prefix          = "${local.name_tag_prefix}-node-"
  image_id             = var.ami
  instance_type        = var.instance_type
  key_name             = var.key_name
  security_groups      = [aws_security_group.node.id]
  iam_instance_profile = aws_iam_instance_profile.node.name
  user_data            = data.template_file.configure-eventstore-script.rendered

  associate_public_ip_address = true
}

#--------------------------------------------------------------
# Autoscaling Group
#--------------------------------------------------------------

# Launch instances using the temaplate

resource "aws_autoscaling_group" "node" {
  name                 = "asg-${aws_launch_configuration.node.name}"
  launch_configuration = aws_launch_configuration.node.name
  vpc_zone_identifier  = aws_subnet.subnets.*.id
  min_size             = var.elastic_ip_count
  max_size             = var.elastic_ip_count
  load_balancers       = [aws_elb.elb.id]
  health_check_type    = "EC2"

  lifecycle {
    create_before_destroy = true
  }

  tags = [
    {
      key                 = "Name"
      value               = aws_launch_configuration.node.name
      propagate_at_launch = true
    },
    {
      key                 = "environment"
      value               = var.environment
      propagate_at_launch = "true"
    },
    {
      key                 = "stack"
      value               = var.stack
      propagate_at_launch = "true"
    },
    {
      key                 = "application"
      value               = var.application
      propagate_at_launch = "true"
    },
    {
      key                 = "created_by"
      value               = "terraform"
      propagate_at_launch = "true"
    },
  ]
  # make sure our log group exists before we try and write to it
  # depends_on = ["aws_cloudwatch_log_group.eventstore"]
}
