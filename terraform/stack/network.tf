#==============================================================
# network.tf
#==============================================================

# Creates a Virtual Private Cloud, Subnets and elastic IP addresses

resource "aws_vpc" "main" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = local.name_tag_prefix
  }
}

#--------------------------------------------------------------
# Internet Gateway
#--------------------------------------------------------------

# To serve traffic we open our VPC to the internet

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = {
    Name = local.name_tag_prefix
  }
}

resource "aws_route" "outgoing" {
  route_table_id         = aws_vpc.main.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

#--------------------------------------------------------------
# Subnets
#--------------------------------------------------------------

# Use count to generate a subnet for each availability zone
# Each Subnet is in a seperate Avaialability Zone for redundancy

resource "aws_subnet" "subnets" {
  count             = length(var.availability_zones)
  vpc_id            = aws_vpc.main.id
  availability_zone = element(var.availability_zones, count.index)
  cidr_block        = element(var.subnet_cidrs, count.index)

  tags = {
    Name = "${local.name_tag_prefix}-${element(var.availability_zones, count.index)}"
  }
}

#--------------------------------------------------------------
# Elastic IP Addresses
#--------------------------------------------------------------

# These IP Addresses are used as static points to connect to our
# In our userdata we assign one of the un-used IPs to our instance

resource "aws_eip" "eips" {
  count = var.elastic_ip_count
  vpc   = true

  tags = {
    Name = "${local.name_tag_prefix}-${count.index}"
  }
}
