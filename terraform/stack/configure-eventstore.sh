#!/usr/bin/env bash

exec > /var/log/user-data.log 2>&1

set -e

# certificate valid for around 68 years
openssl req -x509 -sha256 -nodes -days 24818 -subj "/CN=${tcp_dns}" \
  -newkey rsa:2048 -keyout eventstore.pem -out eventstore.csr

openssl pkcs12 -export -inkey eventstore.pem -in eventstore.csr -out eventstore.p12 -passout pass:

cp eventstore.csr /usr/local/share/ca-certificates/eventstore.crt
cp eventstore.p12 /usr/local/share/ca-certificates/

update-ca-certificates

cert-sync eventstore.csr

rm -f eventstore.pem

# Generate script to fetch latest backup info
cat > /usr/local/bin/fetch-latest-backup-info.sh << 'EOF'
#!/usr/bin/env bash
set -e
json=$(aws s3api list-objects --bucket "${backup_bucket}")
jq '.Contents | max_by(.LastModified) | {key: .Key, lastModified: .LastModified}' <<< "$json"
EOF
chmod u+x /usr/local/bin/fetch-latest-backup-info.sh

#==============================================================
# Use ZFS for the EventStore database in /var/lib/eventstore
#==============================================================
sgdisk -Zg -n1:0:0 -t1:BF01 -c1:ZFS /dev/${database_disk_device}

zpool create \
    -o ashift=12 \
    -o cachefile=/etc/zfs/zpool.cache \
    -O canmount=off \
    -O compression=lz4 \
    -O atime=off \
    -O normalization=formD \
    -m none \
    rpool /dev/${database_disk_partition}

zfs create \
    -o setuid=off \
    -o mountpoint=/var/lib/eventstore \
    rpool/var-lib-eventstore

#==============================================================
# restore from backup
#==============================================================
cd /var/lib/eventstore

# Get the latest backup
KEY=$(/usr/local/bin/fetch-latest-backup-info.sh | jq -r '.key')

if [ -n "$KEY" ]; then
    aws s3 cp s3://${backup_bucket}/$KEY ./backup.tar.gz

    # apply backup
    tar -xvzf ./backup.tar.gz
    rm -f ./backup.tar.gz
    cp -f chaser.chk truncate.chk
fi

# Grab an EIP, taking into account that because of https://github.com/skymill/aws-ec2-assign-elastic-ip/issues/24,
# it may take multiple attempts.

read -r -a eips <<< "${eips}"
eipList=$(printf ",%s" "$${eips[@]}")
eipList=$${eipList:1}

publicIp=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)

# Synchonise all nodes so that they attempt to grab an EIP at roughly the same
# time to increase the likelyhood of the race condition in aws-ec2-assign-elastic-ip.
# Useful only while investigating the race condition.
# currentTime=$(date +%s)
# secondsToSync=$((300 - currentTime % 300))
# echo "Sleeping $secondsToSync seconds to the nearest 5-minute mark"
# sleep $secondsToSync

while [[ ! " $${eips[*]} " == *" $publicIp "* ]]; do
    echo "Public IP $publicIp is not an EIP"
    echo "Attemping to grab one of $eipList"
    result=1
    while [ $result != 0 ]; do
        /usr/local/bin/aws-ec2-assign-elastic-ip --valid-ips "$eipList"
        result=$?
    done;

    newPublicIp=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
    while [[ $newPublicIp == "$publicIp" ]]; do
        echo "Public IP is still $publicIp"
        sleep 1
        newPublicIp=$(curl http://169.254.169.254/latest/meta-data/public-ipv4)
    done
    publicIp=$newPublicIp
    echo "New public IP is $publicIp"
done

IP=$(hostname -I)
export IP

cat > /etc/eventstore/eventstore.conf << EOF
RunProjections: All
ExtIp: $${IP}
IntIp: $${IP}
ExtIpAdvertiseAs: $${publicIp}
IntIpAdvertiseAs: $${publicIp}
ExtHttpPort: 80
IntHttpPort: 2112
ExtTcpPort: 8080
IntTcpPort: 1112
ExtHttpPrefixes: http://*:80/
IntHttpPrefixes: http://*:2112/
AddInterfacePrefixes: false
DiscoverViaDns: true
ClusterDns: ${cluster_dns}
ClusterGossipPort: 2112
IntTcpHeartbeatInterval: 5000
IntTcpHeartbeatTimeout: 5000
ExtTcpHeartbeatInterval: 5000
ExtTcpHeartbeatTimeout: 5000
GossipIntervalMs: 2000
GossipTimeoutMs: 2500
CommitTimeoutMs: 3000
PrepareTimeoutMs: 3000
CertificateFile: /usr/local/share/ca-certificates/eventstore.p12
ExtSecureTcpPort: 5432
ClusterSize: $${#eips[@]}
EOF

sed -i 's/User=eventstore/User=root/' /etc/systemd/system/eventstore.service
sed -i 's/Group=eventstore/Group=root/' /etc/systemd/system/eventstore.service
sed -i '/^Type=simple/aLimitNOFILE=100000' /etc/systemd/system/eventstore.service
sed -i '/^Type=simple/aEnvironment=MONO_THREADS_PER_CPU="250"' /etc/systemd/system/eventstore.service

# remove cache for memory logs
rm -rf /var/tmp/aws-mon/

#==============================================================
# create backup script
#==============================================================

cat << 'EOF' >> /tmp/backup.sh
#!/bin/bash

# Tee stdout and stderr to log file
logFile=/var/log/backup/backup.log
exec > >(tee -a "$logFile")
exec 2> >(tee -a "$logFile" >&2)

echo "Starting backup at $(date)" >> $logFile

scriptName=$(basename $${BASH_SOURCE[0]})

# Only run backups on master
state=`curl -s localhost/info | jq -r '.state'`
if [ "$state" != "master" ]; then
    echo "Node is not master, backups must be made on master"
    exit 1
fi

# Guard against concurrent executions
lockFile=/var/run/"$scriptName"

exec {lock}>$lockFile
flock -n $lock
if [[ $? != 0 ]]; then
    echo "Unable to get lock on $lockFile, is backup already running?"
    exit 1
fi

cd /var/lib/eventstore

export LAST_BACKUP=`date --utc +%FT%TZ`
export BACKUP_FILE="$${HOSTNAME}-store-$${LAST_BACKUP}"

# Create Tarball of files (chk must be done first)
tar --warning=no-file-changed -cvf "./$BACKUP_FILE".tar *.chk
tar --warning=no-file-changed --exclude='*.chk' --exclude="*.tar" -uvf "./$BACKUP_FILE".tar .
gzip "./$BACKUP_FILE".tar

# Upload object (up to 5TB) in parts to bucket (n.b. the bucket name is inserted during terraform apply)
aws s3 cp --quiet $BACKUP_FILE.tar.gz s3://${backup_bucket}/$BACKUP_FILE.tar.gz

rm -f "./$BACKUP_FILE".tar.gz

echo "Completed backup at $(date)" >> $logFile
EOF

mv -f /tmp/backup.sh /usr/local/bin/backup.sh
chmod +x /usr/local/bin/backup.sh

# create cron job to run backup at 1:00am (UTC) daily (11:00am in AEST)
mkdir /var/log/backup
chown ubuntu:ubuntu /var/log/backup
(crontab -l 2>/dev/null || true; echo "0 1 * * * /usr/local/bin/backup.sh") | crontab -

#==============================================================
# reboot master node script
#==============================================================

cat << 'EOF' >> /tmp/reboot-master.sh
#!/bin/bash

state=$(curl -s localhost/info | jq -r '.state')

if [ "$state" != "master" ]; then
    echo "Node is not master, exiting"
else
    reboot
fi
EOF

mv -f /tmp/reboot-master.sh /usr/local/bin/reboot-master.sh
chmod +x /usr/local/bin/reboot-master.sh

#==============================================================
# Scavenge every day at 1:45pm.
# We should run operation scavenge periodically to reclaim deleted
# data, but not during periods of expected high throughput.
#==============================================================

cat > /usr/local/bin/scavenge.sh << 'EOF'
#!/usr/bin/env bash

set -e

timeFormat=%Y-%m-%dT%H:%M:%SZ

# time of latest backup in UNIX Epoch time
timeOfLatestBackup=$(date -d $(/usr/local/bin/fetch-latest-backup-info.sh | jq -r .lastModified) +%s)

# The previous scavenge run should have happened 24 hours ago
timeOfPreviousScavenge=$(($(date +%s) - $((24 * 60 * 60))))

if [ $timeOfLatestBackup -gt $timeOfPreviousScavenge ]; then
    echo "$(date +"$timeFormat"): running scavenge"
    password=$$(/usr/local/bin/credstash -r ${region} get ${name_tag_prefix}AdminPassword)
    curl -i -d {} -X POST http://localhost/admin/scavenge -u "admin:$${password}"
else
    echo "$(date +"$timeFormat"): refusing to run scavenge, because no backup was taken since $(date -d @$timeOfLatestBackup +"$timeFormat")"
fi
EOF
chmod u+x /usr/local/bin/scavenge.sh
mkdir /var/log/scavenge
(crontab -l 2>/dev/null || true; echo "45 13 * * * /usr/local/bin/scavenge.sh >> /var/log/scavenge/scavenge.log 2>&1") | crontab -

#==============================================================
# Check for security updates.
#==============================================================

cat > /usr/local/bin/check-for-security-updates.sh << 'EOF'
#!/usr/bin/env bash

# Upload the number of pending EC2 security updates to CloudWatch as a custom metric.

set -e

# Tee stdout and stderr to log file
logFile=/var/log/check-for-security-updates.log
exec > >(tee -a "$logFile")
exec 2> >(tee -a "$logFile" >&2)

# Environment will be spliced in by Terraform.
environment=${environment}

# shellcheck disable=SC1083
namespace=$${environment^}/EventStoreNode

# Update local package database.
apt-get update

# Get number of pending security updates.
updates=$(/usr/lib/update-notifier/apt-check 2>&1 | cut -f2 -d\;)

imageId=$(curl -s http://169.254.169.254/latest/meta-data/ami-id)
instanceId=$(curl -s http://169.254.169.254/latest/meta-data/instance-id)

# Region will be spliced in by Terraform.
export AWS_DEFAULT_REGION=${region}

# Generate CloudWatch metric for the number of pending security updates on this EC2 instance.
aws cloudwatch put-metric-data \
    --namespace "$namespace" \
    --metric-name PendingSecurityUpdates \
    --dimensions InstanceId="$instanceId",ImageId="$imageId" \
    --value "$updates"
EOF
chmod u+x /usr/local/bin/check-for-security-updates.sh
/usr/local/bin/check-for-security-updates.sh
(crontab -l 2>/dev/null || true; echo "00 9 * * * /usr/local/bin/check-for-security-updates.sh") | crontab -

systemctl enable eventstore

# start it
systemctl start eventstore
echo "Node Configured"
