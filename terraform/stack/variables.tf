#==============================================================
# variables.tf
#==============================================================

# Defines input

# Variables can be set here using default, in env vars using
# TF_VAR_variable_name, or injested as a file by creating a 
# terraform.tfvars file

variable "region" {
  default = "ap-southeast-2"
}

variable "key_name" {
  default     = "geodesy-ops-dev-eventstore"
  description = "Name of AWS EC2 key pair"
}

variable "tf_state_bucket" {
  description = "Name of terraform state S3 bucket"
}

variable "tf_state_table" {
  description = "Name of terraform state lock DDB table"
}

variable "environment" {
  description = "Deployment environment"
}

variable "application" {
  description = "Application name"
  default     = "eventstore"
}

variable "stack" {
  description = "Seperates Blue and Green stacks"
  default     = "blue"
}

variable "owner" {
  description = "Application owner"
  default     = "Geodesy Operations"
}

variable "healthcheck" {
  description = "Healthcheck Path"
  default     = "HTTP:80/stats"
}

variable "availability_zones" {
  default     = ["ap-southeast-2a", "ap-southeast-2b", "ap-southeast-2c"]
  description = "List of private AZs"
}

variable "subnet_cidrs" {
  default     = ["10.1.1.0/24", "10.1.2.0/24", "10.1.3.0/24"]
  description = "List of subnet CIDRs"
}

variable "elastic_ip_count" {
  default = 3
}

variable "ami" {
  description = "EventStore AMI ID"
}

variable "instance_type" {
  default = "t2.medium"
}

variable "database_disk_device" {
  default = "xvdf"
}

variable "database_disk_partition" {
  default = "xvdf1"
}

variable "slack_webhook_path" {
}
