#==============================================================
# elb.tf
#==============================================================

# Create a load balancer, security group and DNS entry

resource "aws_security_group" "elb" {
  # Allow HTTP from anywhere
  name   = "${local.name_tag_prefix}-elb"
  vpc_id = aws_vpc.main.id

  #Uncomment this block when you have issued a certificate
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${local.name_tag_prefix}-elb"
  }
}

resource "aws_elb" "elb" {
  name    = "${local.name_tag_prefix}-elb"
  subnets = aws_subnet.subnets.*.id

  listener {
    ssl_certificate_id = aws_acm_certificate.certificate.arn
    instance_port      = "80"
    instance_protocol  = "HTTP"
    lb_port            = "443"
    lb_protocol        = "HTTPS"
  }

  health_check {
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 30
    target              = var.healthcheck
    interval            = 60
  }

  security_groups = [aws_security_group.elb.id]

  tags = {
    Name = "${local.name_tag_prefix}-elb"
  }
}

resource "aws_route53_record" "elb" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = var.stack
  type    = "A"

  alias {
    name                   = aws_elb.elb.dns_name
    zone_id                = aws_elb.elb.zone_id
    evaluate_target_health = true
  }
}
