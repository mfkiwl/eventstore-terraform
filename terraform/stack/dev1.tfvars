# https://bitbucket.org/geoscienceaustralia/eventstore-terraform/addon/pipelines/home#!/results/350
ami = "ami-09a0c6988b5c9ef25" 

key_name = "geodesy-ops-dev-eventstore"

availability_zones = ["ap-southeast-2a", "ap-southeast-2b"]
subnet_cidrs = ["10.1.1.0/24", "10.1.2.0/24"]
elastic_ip_count = 2

instance_type = "t2.small"
