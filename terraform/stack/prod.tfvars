# https://bitbucket.org/geoscienceaustralia/eventstore-terraform/addon/pipelines/home#!/results/414
ami = "ami-0dac496da9a337d61"

key_name = "geodesy-operations-eventstore-prod"
instance_type = "i3.xlarge"
database_disk_device = "nvme0n1"
database_disk_partition = "nvme0n1p1"
