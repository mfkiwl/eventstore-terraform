#!/usr/bin/env bash

set -e

script="$(basename "${BASH_SOURCE[0]}")"

function printUsage {
    cat << EOF
Usage: $script OPTIONS
where OPTIONS are
   -e, --env
        deployment environment, default "dev"
   -d, --dry-run
        run terraform plan, but not apply
EOF
}

while [[ $# -gt 0 ]]; do
    case $1 in
        -e|--env )
            env="$2"
            shift 2
            ;;
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
        ;;
    esac
done

# Inspect the staging domain name (e.g., "staging.dev-eventstore.geodesy.ga.gov.au")
# to find the name of the stack currently in staging (e.g., "blue" or "green")
if [ "$env" != "prod" ];
then
  export zoneName=${env:-dev}-eventstore.geodesy.ga.gov.au.
else
  export zoneName=eventstore.geodesy.ga.gov.au.
fi

export stagingDomainName=staging.${zoneName}
zoneId=$(aws route53 list-hosted-zones | jq -r '.HostedZones[] | select(.Name == env.zoneName) | .Id')
dnsRecordSets=$(aws route53 list-resource-record-sets --hosted-zone-id "$zoneId")
stagingDnsRecordSet=$(jq '.ResourceRecordSets[] | select(.Name == env.stagingDomainName) | select(.AliasTarget.DNSName != null)' <<< "$dnsRecordSets")
stagingStackName=$(jq -r '.AliasTarget.DNSName | split(".") | first' <<< "$stagingDnsRecordSet")

if [ -z "$stagingStackName" ];then
    echo "Error: Could not accurately determine staging"
    exit 1
fi
echo "$stagingStackName is staging, deleting"

pushd stack
export TF_VAR_region=ap-southeast-2
export TF_VAR_application=eventstore
export TF_VAR_environment=${env:-dev}

slackWebhookPath=$(credstash get ga-gnss-notification-slack-webhook-path)
export TF_VAR_slack_webhook_path=$slackWebhookPath

if [[ $env == "prod" || $env == "test" ]];
then
    env_suffix="-prod"
fi

export TF_VAR_tf_state_bucket=geodesy-operations-terraform-state${env_suffix}
export TF_VAR_tf_state_table=geodesy-operations-terraform-state${env_suffix}
export TF_VAR_stack="$stagingStackName"

terraform init \
	-backend-config "bucket=$TF_VAR_tf_state_bucket" \
	-backend-config "dynamodb_table=$TF_VAR_tf_state_table" \
	-backend-config "region=$TF_VAR_region" \
	-backend-config "key=$TF_VAR_application/$TF_VAR_environment/$TF_VAR_stack/terraform.tfstate"\
    -reconfigure
terraform get
terraform plan -destroy -var-file=$TF_VAR_environment.tfvars

if [ -z "$dryRun" ]; then
    terraform destroy -auto-approve -var-file=$TF_VAR_environment.tfvars
fi

popd
