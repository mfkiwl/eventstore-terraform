#!/usr/bin/env bash

set -e

script="$(basename "${BASH_SOURCE[0]}")"
unset dryRun

function printUsage {
    cat << EOF
Usage: $script OPTIONS
where OPTIONS are
   -e, --env
        deployment environment, default "dev"
   -d, --dry-run
        run terraform plan, but not apply
EOF
}

while [[ $# -gt 0 ]]; do
    case $1 in
        -e|--env )
            env="$2"
            shift 2
            ;;
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
            ;;
    esac
done

zone_name="eventstore.geodesy.ga.gov.au."
if [ "$env" != "prod" ];
then
  export record_set_name=${env:-dev}-${zone_name}
else
  export record_set_name=${zone_name}
fi

zoneid=$(aws route53 list-hosted-zones \
    | jq -r " .HostedZones[] \
    | select(.Name == env.record_set_name) \
    | .Id " \
    )

if [ -z "$zoneid" ]; then
    echo "Error: Could not retrieve hosted zone id"
    exit 1
fi

export address="staging".${record_set_name}
staging=$(aws route53 list-resource-record-sets --hosted-zone-id "$zoneid" \
    | jq -r " .ResourceRecordSets[] \
    | select(.Name == env.address) \
    | select(.AliasTarget.DNSName != null) \
    | .AliasTarget.DNSName | split(\".\") \
    | first " \
    )

if [ -z "$staging" ]; then
    echo "Staging could not be determined - defaulting to blue"
    staging="blue"
fi

echo "${staging} is staging, moving ${staging}.${record_set_name} to release"

pushd switch

export TF_VAR_release_stack="$staging"
export TF_VAR_region=ap-southeast-2
export TF_VAR_application=eventstore
export TF_VAR_environment=${env:-dev}

if [[ $env == "prod" || $env == "test" ]];
then
    env_suffix="-prod"
fi

export TF_VAR_tf_state_bucket=geodesy-operations-terraform-state${env_suffix}
export TF_VAR_tf_state_table=geodesy-operations-terraform-state${env_suffix}
export TF_VAR_stack=switch


terraform init \
	-backend-config "bucket=$TF_VAR_tf_state_bucket" \
	-backend-config "dynamodb_table=$TF_VAR_tf_state_table" \
	-backend-config "region=$TF_VAR_region" \
	-backend-config "key=$TF_VAR_application/$TF_VAR_environment/$TF_VAR_stack/terraform.tfstate"
terraform get
terraform plan

if [ -z "$dryRun" ]; then
    terraform apply -auto-approve
fi

popd

