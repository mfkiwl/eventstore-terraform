#!/usr/bin/env bash

set -e

script="$(basename "${BASH_SOURCE[0]}")"
unset dryRun

function printUsage {
    cat << EOF
Usage: $script OPTIONS
where OPTIONS are
   -e, --env
        deployment environment, default "dev"
   -d, --dry-run
        run terraform plan, but not apply
EOF
}

while [[ $# -gt 0 ]]; do
    case $1 in
        -e|--env )
            env="$2"
            shift 2
            ;;
        -d|--dry-run )
            dryRun=true
            shift
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
        ;;
    esac
done

export TF_VAR_region=ap-southeast-2
export TF_VAR_application=eventstore
export TF_VAR_environment=${env:-dev}

if [[ $env == "prod" || $env == "test" ]];
then
    env_suffix="-prod"
fi

export TF_VAR_tf_state_bucket=geodesy-operations-terraform-state${env_suffix}
export TF_VAR_tf_state_table=geodesy-operations-terraform-state${env_suffix}

pushd setup
export TF_VAR_stack=setup
terraform init \
	-backend-config "bucket=$TF_VAR_tf_state_bucket" \
	-backend-config "dynamodb_table=$TF_VAR_tf_state_table" \
	-backend-config "region=$TF_VAR_region" \
	-backend-config "key=$TF_VAR_application/$TF_VAR_environment/$TF_VAR_stack/terraform.tfstate"\
    -reconfigure
terraform get
terraform plan 

if [ -z "$dryRun" ]; then
    terraform apply -auto-approve
fi
popd
