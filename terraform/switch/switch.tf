provider "aws" {
  region  = "ap-southeast-2"
  version = "3.6.0"
}

terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
    }
  }
  required_version = ">= 0.13"

  backend "s3" {
    # Add encryption to the bucket (not overwritten as part of deploy.sh)
    encrypt = true
  }
}

variable "release_stack" {
  default = "blue"
}

locals {
  domain_name = var.environment == "prod" ? "eventstore.geodesy.ga.gov.au" : "${var.environment}-eventstore.geodesy.ga.gov.au"
}

data "aws_route53_zone" "selected" {
  name = local.domain_name
}

locals {
  other_stack = var.release_stack == "blue" ? "green" : "blue"
}

resource "aws_route53_record" "release" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = ""
  type    = "A"

  alias {
    name                   = "${var.release_stack}.${local.domain_name}"
    zone_id                = data.aws_route53_zone.selected.zone_id
    evaluate_target_health = false
  }
}

resource "aws_route53_record" "staging" {
  zone_id = data.aws_route53_zone.selected.zone_id
  name    = "staging"
  type    = "A"

  alias {
    name                   = "${local.other_stack}.${local.domain_name}"
    zone_id                = data.aws_route53_zone.selected.zone_id
    evaluate_target_health = false
  }
}
