#==============================================================
# variables.tf
#==============================================================

# Defines input

# Variables can be set here using default, in env vars using
# TF_VAR_variable_name, or injested as a file by creating a 
# terraform.tfvars file

variable "environment" {
  description = "Deployment environment"
}
