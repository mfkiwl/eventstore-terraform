{ sources ? import ./sources.nix }:

import sources.nixpkgs {
  overlays = [
    (pkgs: _: {
      inherit (import sources.niv { inherit pkgs; }) niv;
    })
  ];
}
