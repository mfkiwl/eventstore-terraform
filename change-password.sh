#!/usr/bin/env bash

script="$(basename "${BASH_SOURCE[0]}")"

function printUsage {
    cat << USAGE
Usage: $script --environment <env> [--new-password <password>] [--help]

Change the EventStore admin user password and save it to credstash
under '<env>-eventstoreAdminPassword'. Run the EventStore backup script to
ensure the new password is backed up immediately.

Options

  -e, --environment
      "dev", "dev1", "test", or "prod"

   --new-password <password>
       optional new password for the admin user,
       use a randomly generated password if omitted

  -h, --help
       print this help message
USAGE
}

if [[ $# -eq 0 ]]; then
    printUsage
    exit 0
fi

while [[ $# -gt 0 ]]; do
    case $1 in
        -e|--environment )
            environment="$2"
            shift 2
            if [[ $environment != "dev" && $environment != "dev1" && $environment != "test" && $environment != "prod" ]]; then
                printUsage
                exit 1
            fi
            ;;
         --new-password )
            newPassword="$2"
            shift 2
            if [ -z "${newPassword}" ]; then
                echo "No password supplied"
                exit 1
            fi
            ;;
        -h|--help )
            printUsage
            exit 0
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
            ;;
    esac
done

credstashKey="${environment}-eventstoreAdminPassword"
if [ "${environment}" == "prod" ];
then
  eventStoreUrl="https://eventstore.geodesy.ga.gov.au"
else
  eventStoreUrl="https://${environment}-eventstore.geodesy.ga.gov.au"
fi

function attemptWrite {
    # Try a write, output the HTTP response code, fail > 400
    attempts=0
    written=1
    until [[ $attempts == 10 ]]; do
        httpCode=$(curl -f -s -o /dev/null -w "%{http_code}" \
            -d "{}" "$eventStoreUrl/streams/test" \
            -u "admin:$1" \
            -H "Content-Type:application/json" \
            -H "ES-EventType: SomeEvent" \
            -H "ES-EventId: B322E299-CB73-4B47-97C5-5054F920746F")
        rc=$?
        if [[ $rc != 0 ]]; then
            echo "HTTP $httpCode, failed to write to EventStore (attempt $((attempts + 1)) of 10)"
            attempts=$((attempts + 1))
            sleep 1
        else
            written=0
            attempts=10
        fi
    done
    return $written
}

# Get the password, if it doesn't exist use default
oldPassword=$(credstash get "$credstashKey")
rc=$?;
if [[ $rc != 0 ]]; then
    oldPassword="changeit"
fi

attemptWrite "$oldPassword"
canWrite=$?

if [[ -z "${newPassword}" ]]; then
    newPassword=$(head /dev/urandom | tr -dc A-Za-z0-9\!\@\#\$%^\&\*\(\)_\+ | head -c 20)
fi

function stashPassword() {
    credstash put -a "$credstashKey" "$1" > /dev/null
}

function stashNewPassword() {
    stashPassword "$newPassword"
    echo "Saved new password to credstash"
}

function restoreOldPassword() {
    stashPassword "$oldPassword"
    echo "Restored old password to credstash"
}

stashNewPassword

# Try to change the password
passwordChanged=false

attempts=0
until [[ $attempts == 10 ]]; do
    httpCode=$(curl -f -s -o /dev/null -w "%{http_code}" \
        -u "admin:$oldPassword" \
        "$eventStoreUrl/users/admin/command/reset-password" \
        -H "Content-type: application/json" \
        -d "{\"newPassword\":\"$newPassword\"}")

    rc=$?;
    if [[ $rc != 0 ]]; then
        echo "HTTP $httpCode, failed to change EventStore password (attempt $((attempts + 1)) of 10)"
        attempts=$((attempts + 1))
        sleep 1
    else
        echo "EventStore admin password changed"
        passwordChanged=true
        attempts=10
    fi
done

# Check if password was changed
if [[ $passwordChanged == false ]]; then
    echo "Failed to change EventStore password"
    restoreOldPassword
    exit 1
fi

attemptWrite "$newPassword"
canWrite=$?

if [[ $canWrite != 0 ]]; then
    echo "Failed to connect to EventStore using the new password"
    restoreOldPassword
    exit 1
fi


# Trigger backup

scriptName="${environment}-backup_script"
aws ssm send-command --document-name "$scriptName" \
    --targets \
        Key=tag:environment,Values="${environment}" \
        Key=tag:application,Values=eventstore > /dev/null
rc=$?

if [[ "$rc" != 0 ]]; then
    echo "Failed to trigger EventStore backup"
    exit 1
fi

echo "Triggered EventStore backup"

# TODO: Don't wait for backups to complete until we have implemented
# incremental backups Check if it worked

# instanceIds=$(aws ec2 describe-instances --filters \
#     Name=tag:application,Values=eventstore \
#     Name=tag:environment,Values="${environment}" \
#     | jq -r '..|.InstanceId? | select(. != null)')

# attempts=0
# until [[ $attempts == 10 ]]; do
#     for instance in $instanceIds; do
#         result=$(aws ssm get-command-invocation \
#             --command-id "$commandId" --instance-id "$instance")
#         status=$(echo "$result" | jq -r '.Status')
#         if [[ $status == "Success" ]]; then
#             echo "done"
#             exit 0
#         fi
#     done

#     echo -n "."
#     attempts=$((attempts + 1))
#     sleep 10
# done
# echo
# echo "Error: backup failed"
# exit 1
