#!/usr/bin/env bash

script="$(basename "${BASH_SOURCE[0]}")"

function printUsage {
    cat << USAGE
Usage: $script --environment <env> [--help]

Reboot master node

Options

  -e, --environment
      "dev", "dev1", "test", or "prod"

  -h, --help
       print this help message
USAGE
}

if [[ $# -eq 0 ]]; then
    printUsage
    exit 0
fi

while [[ $# -gt 0 ]]; do
    case $1 in
        -e|--environment )
            environment="$2"
            shift 2
            if [[ $environment != "dev" && $environment != "dev1" && $environment != "test" && $environment != "prod" ]]; then
                printUsage
                exit 1
            fi
            ;;
        -h|--help )
            printUsage
            exit 0
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
            ;;
    esac
done

scriptName="${environment}-eventstore-reboot-master-script"
aws ssm send-command --document-name "$scriptName" \
    --targets \
        Key=tag:environment,Values="${environment}" \
        Key=tag:application,Values=eventstore > /dev/null
rc=$?

if [[ "$rc" != 0 ]]; then
    echo "Failed to invoke remote script"
    exit 1
fi
