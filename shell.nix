{
  pkgs ? import ./nix {},
  installDevTools ? true
}:

let
  s3wipe = pkgs.pythonPackages.buildPythonPackage rec {
    pname = "s3wipe";
    version = "0.3";

    src = pkgs.pythonPackages.fetchPypi {
      inherit pname version;
      sha256 = "0y32mvcy11ycycskhv2m1l81bgbyfcdgdw4w76j6cacjxrmvfwdb";
    };

    propagatedBuildInputs = [ pkgs.pythonPackages.boto ];
    doCheck = false;

    meta = {
      homepage = "http://github.com/eschwim/s3wipe";
      description = "Rapid AWS S3 bucket delete tool";
      licenses = pkgs.licenses.mit;
    };
  };

  buildTools = with pkgs; [
    awscli
    cacert
    jq
    git
    terraform_0_13
    packer
    s3wipe
    pythonPackages.credstash
    zip
    (python3.withPackages(ps: with ps; [boto3]))
  ];

  devTools = with pkgs; [
    niv
  ];

  env = with pkgs; buildEnv {
    name = "devEnv";
    paths = buildTools ++ (
      if installDevTools then devTools else []
    );
  };

in
  pkgs.runCommand "setupEnv" {
    buildInputs = [
      env
    ];
    shellHook = ''
      export SSL_CERT_FILE="${pkgs.cacert}/etc/ssl/certs/ca-bundle.crt"

      if [ -e ./terraform/aws-env.sh ]; then
        . ./terraform/aws-env.sh
      fi
    '';
  } ""
