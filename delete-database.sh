#!/usr/bin/env bash
set -e

script="$(basename "${BASH_SOURCE[0]}")"

function printUsage {
    cat << USAGE

Usage:      $script OPTIONS

where OPTIONS are

    -e, --environment, "dev", "dev1" or "test"
        environment to delete database, default "dev"

    -h, --help
        print this help message

USAGE
}

if [[ $# -eq 0 ]]; then
    printUsage
    exit 0
fi

while [[ $# -gt 0 ]]; do
    case $1 in
        -e|--environment )
            environment="$2"
            shift 2
            if [[ $environment != "dev" && $environment != "dev1" && $environment != "test" ]]; then
                printUsage
                exit 1
            fi
            ;;
        -h|--help )
            printUsage
            exit 0
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
            ;;
    esac
done

echo "Destroying stacks"

record_set_name="${environment}-eventstore.geodesy.ga.gov.au."
zoneid=$(aws route53 list-hosted-zones --query "HostedZones[?Name == '${record_set_name}'].Id" --output text)
get_stack() {
    address="$1"
    stack=$(aws route53 list-resource-record-sets \
        --hosted-zone-id "${zoneid}" \
        --query "ResourceRecordSets[?( \
                Name == '${address}' && \
                AliasTarget.DNSName != null)] \
            .AliasTarget.DNSName" \
         --output text | cut -d'.' -f1)
    echo "${stack}"
}

destroy_stack() {
    stack_name="$1"
    pushd terraform/stack
    export TF_VAR_region=ap-southeast-2
    export TF_VAR_application=eventstore
    export TF_VAR_environment=${environment}

    export TF_VAR_tf_state_bucket=geodesy-operations-terraform-state
    export TF_VAR_tf_state_table=geodesy-operations-terraform-state
    export TF_VAR_stack="$stack_name"

    terraform init \
        -backend-config "bucket=$TF_VAR_tf_state_bucket" \
        -backend-config "dynamodb_table=$TF_VAR_tf_state_table" \
        -backend-config "region=$TF_VAR_region" \
        -backend-config "key=$TF_VAR_application/$TF_VAR_environment/$TF_VAR_stack/terraform.tfstate"\
        -reconfigure
    terraform get
    terraform destroy -auto-approve -var-file=$TF_VAR_environment.tfvars
    popd
}

staging_stack=$(get_stack "staging."${record_set_name})
destroy_stack ${staging_stack} || true

release_stack=$(get_stack ${record_set_name})
destroy_stack ${release_stack} || true

echo "Deleting backups"
account_id=$(aws sts get-caller-identity --output text --query 'Account')
aws s3 rm --recursive s3://${environment}-eventstore-backups-${account_id}

echo "Deleting permanent storage"
time s3wipe --path s3://geodesy-archive-permanent-storage-${environment}

echo "Starting nodes"
pushd terraform
./deploy-stack.sh -e ${environment} -s blue
./deploy-stack.sh -e ${environment} -s green
./switch-primary-stack.sh -e test
popd
echo "Destroying staging stack"
staging_stack=$(get_stack "staging."${record_set_name})
destroy_stack ${staging_stack} || true


admin_username=$(credstash get ${environment}-eventstoreAdminUsername)
admin_password=$(credstash get ${environment}-eventstoreAdminPassword)

# set current password to the stack's default password
credstash put -a ${environment}-eventstoreAdminPassword changeit

echo "Changing eventstore password"
./terraform/changePassword.sh -e ${environment} --new-password ${admin_password}

echo "Resetting projections"
./enable-projection.sh -e ${environment}
