#!/usr/bin/env bash
set -e

script="$(basename "${BASH_SOURCE[0]}")"

function printUsage {
    cat << USAGE

Usage:      $script OPTIONS

where OPTIONS are

    -e, --environment, "test" or "dev"
        environment to reset projection, default "dev"

    -h, --help
        print this help message

USAGE
}

if [[ $# -eq 0 ]]; then
    printUsage
    exit 0
fi

while [[ $# -gt 0 ]]; do
    case $1 in
        -e|--environment )
            environment="$2"
            shift 2
            if [[ $environment != "dev" && $environment != "test" ]]; then
                printUsage
                exit 1
            fi
            ;;
        -h|--help )
            printUsage
            exit 0
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
            ;;
    esac
done

admin_username=$(credstash get ${environment}-eventstoreAdminUsername)
admin_password=$(credstash get ${environment}-eventstoreAdminPassword)

if [ -z $admin_password ]; then
    admin_password="changeit"
fi

url="https://${environment}-eventstore.geodesy.ga.gov.au/"
curl -s -o /dev/null \
    -u "${admin_username}:${admin_password}" \
    "$url/projection/\$by_category/command/enable" \
    -X POST

