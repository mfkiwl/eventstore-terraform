# EventStore Terraform

Deploy an [EventStore](https://github.com/EventStore/EventStore) cluster into AWS using Terraform.

* Ubuntu 18.04
* EventStore 4.1.1-hotfix1

### Requirements
* packer
* terraform

#### Building a Development Environment

This project uses [Nix](https://nixos.org/nix) to manage build environments
for both development workstations and BitBucket Pipelines. See
geoscienceaustralia/project0-nix.

1) Install Nix

```bash
curl https://nixos.org/nix/install | sh
```

2) Enter nix shell to download and install the required software as specified in `./shell.nix`

```bash
nix-shell
```

#### Managing Nix Dependencies

Edit `shell.nix` to add or remove dependencies. Use
[Niv](https://github.com/nmattia/niv) to manage
[nixpkgs](https://github.com/NixOS/nixpkgs) and any other sources of nix
expressions. Regularly cache any changes to project dependencies by manually
running custom pipeline `build-pipelines-docker-image`.

#### Initial deploy to Dev
```bash
cd terraform
export AWS_ACCESS_KEY_ID=???
export AWS_SECRET_ACCESS_KEY=???

# Deploy setup, both stacks, and run switch to set a primary
./deploy-setup.sh -e dev
./deploy-stack.sh -e dev -s blue
./deploy-stack.sh -e dev -s green
./switch-primary-stack.sh -e dev

# Destroy staging
./destroy-staging.sh -e dev
```

#### Deployment lifecycle
```bash
cd terraform
export AWS_ACCESS_KEY_ID=???
export AWS_SECRET_ACCESS_KEY=???

# Deploy to staging (-s default is 'staging')
./deploy-stack.sh -e dev -s staging

# Test staging (manual) and then switch the release stack
./switch-primary-stack.sh -e dev

# Destroy the new staging stack
./destroy-staging.sh -e dev
```
