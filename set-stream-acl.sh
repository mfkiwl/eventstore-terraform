#!/usr/bin/env bash

set -e

AWS_REGION=ap-southeast-2
settings_json=acl-settings.json

function printUsage {
    cat << EOF
Usage: $(basename "$0") <env>
where
   <env>
        mandatory environment, eg., dev, test, prod
EOF
}

while [[ $# -gt 0 ]]; do
    case $1 in
        dev|test|prod )
            env="$1"
            shift 1
            ;;
        * )
            echo "Unknown option: $1"
            printUsage
            exit 1
        ;;
    esac
done

if [[ -z $env ]]; then
    echo "Error: unspecified environment"
    printUsage
    exit 1
fi

if [ "$env" == "prod" ]; then env_prefix=""; else env_prefix="${env}-"; fi

url=http://nodes.${env_prefix}eventstore.geodesy.ga.gov.au
username=$(credstash -r "$AWS_REGION" get "${env}-eventstoreAdminUsername")
password=$(credstash -r "$AWS_REGION" get "${env}-eventstoreAdminPassword")

curl -XPOST -i -d @${settings_json} "${url}/streams/%24settings" -H "Content-Type:application/json" \
    -H "ES-EventType:settings" -H "ES-EventId: $(uuidgen)" -u "${username}:${password}"
