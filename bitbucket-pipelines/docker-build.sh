#!/usr/bin/env bash

set -euxo pipefail

organization=geoscienceaustralia
repository=eventstore-terraform-pipeline
username=geodesyarchive
tag=latest

# TODO: Rename geodesy_archive to gnss_informatics
passwordKey=geodesy_archive_docker_hub_key

script="$(basename "${BASH_SOURCE[0]}")"
function usage {
    echo "Usage: $script [-t tag]
        where
        [-t tag]
            optional tag of the new image to be created, default is latest"
    exit 1
}

while [[ $# -gt 0 ]]; do
    case $1 in
        -t )
            tag=$2
            shift 2
            ;;
        * )
            echo "Unknown option: $1"
            usage
            ;;
    esac
done

cd "$(dirname "${BASH_SOURCE[0]}")"

credstash get "$passwordKey" | docker login -u $username --password-stdin
docker build -t "$organization/$repository:$tag" -f Dockerfile ..
docker push "$organization/$repository:$tag"
