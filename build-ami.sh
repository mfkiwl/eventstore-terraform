#!/usr/bin/env bash

(cd ami && GIT_URL=$(git remote get-url origin) GIT_REVISION=$(git rev-parse HEAD) packer build build.json)
